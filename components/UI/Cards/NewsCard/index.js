import { useState, useEffect } from 'react';

import DateTimeHelper from 'helpers/DateTime';

import { Button } from 'components/UI';

import styles from './styles.scss';

const NewsCard = ({ news, isOpen}) => {
  const [mounded, setMounted] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setMounted(true);
  })
  useEffect(() => {
    if (mounded) {
      setOpen(isOpen);
    }
  }, [isOpen]);

  return (
    <div id={`article-${news.id}`} className="card-wrapper">
      <div className="news-card">
        <div className="news-card__header" style={{ backgroundImage: `url(img/articles/promo/${news.promo_img})` }}>
          <span className="news-card__time">
            {DateTimeHelper.getFormattedDate(news.date)}
          </span>
          <h2 className="news-card__title">
            {news.title}
          </h2>
        </div>
        <div className="news-card__content">
          <div className={`news-card__text${open ? ' opened' : ''}`}>
            <p>
              {news.promo}
            </p>
            {(news.content)}
          </div>
          <Button caption={open ? 'Скрыть' : 'Подробнее'} onClick={() => setOpen(!open)} />
        </div>
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};
export default NewsCard;
