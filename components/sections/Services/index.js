import { Container, Row } from 'components/UI';

import { services } from 'data';

import dynamic from 'next/dynamic';

import styles from './styles.scss';

const ServiceCard = dynamic(() => import('./Card'));

const ServicesSection = () => (
  <>
    <section className="section-primary light-bg">
      <Container>
        <Row>
          <div className="section-header dark-color">
            <h2 className="section-title">Популярные услуги</h2>
            <p className="section-subtitle">Все услуги вы можете найти в соответсвующей вкладке</p>
          </div>
        </Row>
        <Row>
          <div className="services-grid">
            {services.map((service) => (
              <ServiceCard key={service.id} service={service} />
            ))}
            <ServiceCard service={{
              id: 10000,
              title: 'Другие услуги',
              image: {
                name: 'all_services.webp',
                height: 324,
                width: 509,
                alt: 'Все наши услуги',
              },
            }}
            />
          </div>
        </Row>
      </Container>
    </section>
    <style jsx>{styles}</style>
  </>
);
export default ServicesSection;
