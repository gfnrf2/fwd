import Image from 'next/image';

import styles from './styles.scss';

const ServiceCard = ({ service }) => {
  const { id, title, image } = service;

  return (
    <>
      <a className="service-card" href={id === 10000 ? '/services' : `/services?page=${Math.ceil(id / 2)}#service-${id}`} title={image.alt}>
        <div className="service-card__image-wrapper">
          <div>
            <img className="service-card__image" src={`/img/services/${image.name}`} height={image.height} width={image.width} alt={image.alt || title} />
          </div>
        </div>
        <div className="service-card__text-wrapper">
          <h3 className="service-card__title">{title}</h3>
        </div>
      </a>
      <style jsx>{styles}</style>
    </>
  );
};
export default ServiceCard;
