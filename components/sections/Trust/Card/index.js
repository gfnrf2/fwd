import Image from 'next/image';

import styles from './styles.scss';

const MarkCard = ({ mark, open }) => {
  const { img } = mark;
  return (
    <>
      <div className={`mark-card${open ? ' open' : ''}`} title={img.alt}>
        <div>
          <img
            src={`/img/marks/${img.name}`}
            height={img.height}
            width={img.width}
            alt={`Работаем с автомобилями модели ${img.alt}`}
            />
        </div>
      </div>
      <style jsx>{styles}</style>
    </>
  );
};
export default MarkCard;
