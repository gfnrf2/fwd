const site = {
  author: 'Lu Perfect | Никита Иванович Лукашев | vk.com/lu.perfect',
  prefix: 'fwd',
  seo: {
    title: 'Полный привод',
    description: 'Полный привод - услуги по ремонту авто.',
    keywords: 'Ремонт, авто, СТО',
  },
  links: [
    {
      id: 1,
      caption: 'Главная',
      href: '/',
    },
    {
      id: 2,
      caption: 'Услуги',
      href: '/services',
    },
    {
      id: 3,
      caption: 'Статьи',
      href: '/articles',
    },
    {
      id: 4,
      caption: 'Адрес',
      href: '/address',
    },
  ],
  contacts: {
    phone: '+7 (952) 667-72-58',
  },
};

export default site;
