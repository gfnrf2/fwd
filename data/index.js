import services from './services';
import articles from './articles';
import marks from './marks';
import site from './site';

export {
  site, services, marks, articles
};
