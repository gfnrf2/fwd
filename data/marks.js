const marks = [
  {
    id: 1,
    img: {
      name: 'uaz.webp',
      width: 126,
      height: 72,
      alt: 'УАЗ',
    },
  },
  {
    id: 2,
    img: {
      name: 'mitsubishi.webp',
      width: 96,
      height: 102,
      alt: 'Mitsubishi Motors',
    },
  },
  {
    id: 3,
    img: {
      name: 'toyota.webp',
      width: 102,
      height: 84,
      alt: 'Toyota',
    },
  },
  {
    id: 4,
    img: {
      name: 'nissan.webp',
      width: 90,
      height: 78,
      alt: 'Nissan',
    },
  },
  {
    id: 5,
    img: {
      name: 'volkswagen.webp',
      width: 80,
      height: 80,
      alt: 'Volkswagen',
    },
  },
  {
    id: 6,
    img: {
      name: 'lada.webp',
      width: 102,
      height: 74,
      alt: 'Lada',
    },
  },
  {
    id: 7,
    img: {
      name: 'volvo.webp',
      width: 78,
      height: 78,
      alt: 'Volvo',
    },
  },
  {
    id: 8,
    img: {
      name: 'suzuki.webp',
      width: 96,
      height: 64,
      alt: 'Suzuki',
    },
  },
  {
    id: 9,
    img: {
      name: 'subaru.webp',
      width: 116,
      height: 70,
      alt: 'Subaru',
    },
  },
  {
    id: 10,
    img: {
      name: 'skoda.webp',
      width: 68,
      height: 80,
      alt: 'Skoda',
    },
  },
  // Hidden
  {
    id: 11,
    img: {
      name: 'audi.webp',
      width: 106,
      height: 66,
      alt: 'Audi',
    },
  },
  {
    id: 12,
    img: {
      name: 'bmw.webp',
      width: 80,
      height: 80,
      alt: 'BMW',
    },
  },
  {
    id: 13,
    img: {
      name: 'ford.webp',
      width: 103,
      height: 40,
      alt: 'Ford',
    },
  },
  {
    id: 14,
    img: {
      name: 'honda.webp',
      width: 96,
      height: 63,
      alt: 'Honda',
    },
  },
  {
    id: 15,
    img: {
      name: 'hyundai.webp',
      width: 92,
      height: 58,
      alt: 'Hyundai',
    },
  },
  {
    id: 16,
    img: {
      name: 'mazda.webp',
      width: 67,
      height: 68,
      alt: 'Mazda',
    },
  },
  {
    id: 17,
    img: {
      name: 'opel.webp',
      width: 93,
      height: 72,
      alt: 'Opel',
    },
  },
  {
    id: 18,
    img: {
      name: 'peugeot.webp',
      width: 91,
      height: 65,
      alt: 'Peugeot',
    },
  },
  {
    id: 19,
    img: {
      name: 'renault.webp',
      width: 57,
      height: 70,
      alt: 'Renault',
    },
  },
  {
    id: 20,
    img: {
      name: 'seat.webp',
      width: 80,
      height: 64,
      alt: 'Seat',
    },
  },
];
export default marks;
